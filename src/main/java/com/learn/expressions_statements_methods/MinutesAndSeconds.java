package com.learn.expressions_statements_methods;

/**
 * Hello world!
 *
 */
public class MinutesAndSeconds {

	private static final String INVALID_STRING = "Invalid Value";

	public static void main(String[] args) {

		System.out.println(getDurationString(65, 45));
		System.out.println(getDurationString(3945));
		System.out.println(getDurationString(-41));
		System.out.println(getDurationString(65, 9));
	}

	public static String getDurationString(int seconds) {

		if (seconds >= 0) {
			int minutes = seconds / 60;
			int remainingSeconds = seconds % 60;

			return getDurationString(minutes, remainingSeconds);
		} else {
			return INVALID_STRING;
		}
	}

	private static String getDurationString(int minutes, int seconds) {

		if (minutes >= 0 && seconds >= 0 && seconds <= 59) {
			int minutesWithSeconds = minutes;
			int hours = minutesWithSeconds / 60;
			int remainingMinutes = minutesWithSeconds % 60;

			return String.format("%dh %dm %ds", hours, remainingMinutes, seconds);
		} else {
			return INVALID_STRING;
		}
	}
}
