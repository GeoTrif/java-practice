package com.learn.oop1;

import java.util.Scanner;

public class BankAccountTest {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		BankAccount bankAccount = new BankAccount();

		bankAccount.setAccountNumber(12345678910L);
		bankAccount.setBalance(2000);
		bankAccount.setCustomerName("Geo Trif");
		bankAccount.setEmail("geotrif@yahoo.com");
		bankAccount.setPhoneNumber("0734345646");

		System.out.println("Account number: " + bankAccount.getAccountNumber());
		System.out.println("Balance: " + bankAccount.getBalance());
		System.out.println("Customer name: " + bankAccount.getCustomerName());
		System.out.println("Email: " + bankAccount.getEmail());
		System.out.println("Phone number: " + bankAccount.getPhoneNumber());

		System.out.println("Enter the amount of money you want to deposit: ");
		if (scanner.hasNextDouble()) {
			double amount = scanner.nextDouble();
			bankAccount.depositFunds(amount);
			System.out.println("You now have " + bankAccount.getBalance() + " $ in your account.");
		} else {
			System.out.println("Invalid input.");
		}

		System.out.println("Enter teh amount of money you want to withdraw: ");
		if (scanner.hasNextDouble()) {
			double amount = scanner.nextDouble();
			System.out.println("Amount withdrawn : " + bankAccount.withdrawFunds(amount));
			System.out.println("You currently have " + bankAccount.getBalance() + " $ in your account.");
		}

		scanner.close();
	}
}