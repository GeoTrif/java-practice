package com.learn.control_flow_statements;

public class SwitchStatement {

	public static void main(String[] args) {
		charSwitchStatement('F');

	}

	public static void charSwitchStatement(char chr) {
		switch (chr) {
		case 'A':
		case 'B':
		case 'C':
		case 'D':
		case 'E':
			System.out.println("Was an A,B,C,D or E.");
			System.out.println("Actually it was " + chr);
			break;

		default:
			System.out.println("Not found");
		}
	}
}
