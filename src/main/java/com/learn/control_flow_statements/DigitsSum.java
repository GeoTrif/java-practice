package com.learn.control_flow_statements;

public class DigitsSum {

	public static void main(String[] args) {

		System.out.println(sumDigits(1));
		System.out.println(sumDigits(125));
		System.out.println(sumDigits(-23));
		System.out.println(sumDigits(56));
		System.out.println(sumDigits(32123));

	}

	public static int sumDigits(int number) {
		int sum = 0;

		if (number >= 10) {
			while (number % 10 != 0) {
				int digit = number % 10;
				number /= 10;
				sum += digit;
			}
			return sum;
		} else {
			return -1;
		}
	}
}
