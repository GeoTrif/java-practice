package com.learn.control_flow_statements;

public class WhileAndDoWhile {

	public static void main(String[] args) {

		int number = 4;
		int finishNumber = 20;
		int counter = 0;

		while (number <= finishNumber) {
			number++;
			if (!isEvenNumber(number)) {
				continue;
			}

			if (counter == 5) {
				break;
			}
			counter++;

			System.out.println("Even number " + number);
		}

		System.out.println("Total even numbers found: " + counter);
	}

	public static boolean isEvenNumber(int number) {
		if (number % 2 == 0) {
			return true;
		} else {
			return false;
		}
	}
}
