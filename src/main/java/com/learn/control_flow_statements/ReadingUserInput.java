package com.learn.control_flow_statements;

import java.util.Scanner;

public class ReadingUserInput {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter your year of birth: ");
		int years = scanner.nextInt();
		System.out.println("Enter your name: ");
		String name = scanner.next();

		System.out.println(name + ",you are " + (2019 - years) + " years old.");
		scanner.close();
	}
}
