package com.learn.control_flow_statements;

public class ForStatement {

	public static void main(String[] args) {

		int count = 0;

		for (int range = 10; range <= 50; range++) {
			if (count == 10) {
				break;
			}

			if (isPrime(range)) {
				System.out.println(range);
				count++;
			}

		}

	}

	public static boolean isPrime(int n) {

		if (n == 1) {
			return false;
		}

		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}

		return true;
	}
}
