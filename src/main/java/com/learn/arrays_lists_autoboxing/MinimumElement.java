package com.learn.arrays_lists_autoboxing;

import java.util.Arrays;
import java.util.Scanner;

public class MinimumElement {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Enter the number of elements of the array:");
		int numberOfElements = 0;

		if (scanner.hasNextInt()) {
			numberOfElements = scanner.nextInt();
		} else {
			System.out.println("Invalid data.");
		}

		scanner.nextLine();

		int[] array = readIntegers(numberOfElements);

		System.out.println("The array is = " + Arrays.toString(array));

		System.out.println("The minimum is: " + findMin(array));

	}

	public static int[] readIntegers(int count) {
		int[] array = new int[count];

		for (int i = 0; i < array.length; i++) {
			System.out.println("Enter the element " + i + " :");

			if (scanner.hasNextInt()) {
				array[i] = scanner.nextInt();
			} else {
				System.out.println("Invalid data.");
			}
			scanner.nextLine();
		}

		return array;
	}

	public static int findMin(int[] array) {
		int min = Integer.MAX_VALUE;

		for (int i = 0; i < array.length; i++) {
			if (array[i] < min) {
				min = array[i];
			}
		}

		if (min == Integer.MAX_VALUE) {
			min = 0;
		}

		return min;
	}
}
