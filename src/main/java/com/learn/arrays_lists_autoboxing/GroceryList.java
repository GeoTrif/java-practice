package com.learn.arrays_lists_autoboxing;

import java.util.ArrayList;

/**
 * CRUD operations on an ArrayList.
 * 
 * @author geotrif
 *
 */
public class GroceryList {

	private ArrayList<String> groceryList = new ArrayList<String>();

	public ArrayList<String> getGroceryList() {
		return groceryList;
	}

	// Create
	public void addGroceryItem(String item) {
		groceryList.add(item);
	}

	// Read
	public void printGroceryList() {
		System.out.println("You have " + groceryList.size() + " items in your grocery list");
		for (String item : groceryList) {
			System.out.println((groceryList.indexOf(item) + 1) + ". " + item);
		}
	}

	public void modifyGroceryItem(String currentItem, String newItem) {
		int position = findItem(currentItem);

		if (position >= 0) {
			modifyGroceryItem(position, newItem);
		}
	}

	// Update
	private void modifyGroceryItem(int position, String newItem) {
		groceryList.set(position, newItem);
		System.out.println("Grocery item " + (position + 1) + " has been modified.");
	}

	public void removeGroceryItem(String item) {
		int position = findItem(item);

		if (position >= 0) {
			removeGroceryItem(position);
		}
	}

	// Delete
	private void removeGroceryItem(int position) {
		groceryList.remove(position);
	}

	// Search
	private int findItem(String searchItem) {
		return groceryList.indexOf(searchItem);
	}

	public boolean onFile(String searchItem) {
		int position = findItem(searchItem);

		if (position >= 0) {
			return true;
		} else {
			return false;
		}
	}
}
