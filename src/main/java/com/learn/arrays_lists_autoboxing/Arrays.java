package com.learn.arrays_lists_autoboxing;

import java.util.Scanner;

/**
 * Create a program using arrays that sorts a list of integers in descending
 * order. Descending order is highest value to lowest. Set up the program that
 * the numbers to sort are read from the keyboard. Implement the following
 * methods - getIntegers,printArray and sortIntegers.
 * 
 * @author GeoTrif
 */
public class Arrays {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Enter the number of numbers:");
		int number = scanner.nextInt();

		int[] myIntArray = getIntegers(number);
		printArray(myIntArray);

		System.out.println("------------------------");

		int[] sortedArray = sortIntegers(myIntArray);
		printArray(sortedArray);

	}

	public static int[] getIntegers(int number) {
		System.out.println("Enter " + number + " integer numbers.");
		int[] array = new int[number];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Enter element " + i);
			array[i] = scanner.nextInt();
		}

		return array;
	}

	public static void printArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("Element " + i + " has value " + array[i]);
		}
	}

	public static int[] sortArrayUsingApi(int[] array) {
		java.util.Arrays.sort(array);
		return array;

	}

	public static int[] sortArrayBubbleSort(int[] array) {
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = 0; j < array.length - i - 1; j++) {
				if (array[j] < array[j + 1]) {
					int temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
				}
			}
		}
		return array;
	}

	public static int[] sortIntegers(int[] array) {
		int[] sortedArray = new int[array.length];
		for (int i = 0; i < array.length; i++) {
			sortedArray[i] = array[i];
		}

		boolean flag = true;
		int temp;

		while (flag) {
			flag = false;
			for (int i = 0; i < sortedArray.length - 1; i++) {
				if (sortedArray[i] < sortedArray[i + 1]) {
					temp = sortedArray[i];
					sortedArray[i] = sortedArray[i + 1];
					sortedArray[i + 1] = temp;
					flag = true;
				}
			}
		}

		return sortedArray;
	}
}