package com.learn.arrays_lists_autoboxing;

import java.util.Arrays;
import java.util.Scanner;

public class ReverseArray {

	private static Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Enter the number of elements:");
		int count = scanner.nextInt();
		int[] array = readIntegers(count);

		System.out.println("Array unreversed = " + Arrays.toString(array));
		reverse(array);
		System.out.println("Array reversed = " + Arrays.toString(array));

//		System.out.println("---------------------");
//
//		System.out.println("Array reversed = " + Arrays.toString(reverseArray(array)));
	}

	public static int[] readIntegers(int count) {
		int[] array = new int[count];

		for (int i = 0; i < array.length; i++) {
			System.out.println("Enter an element:");
			array[i] = scanner.nextInt();
		}

		return array;
	}

	public static void reverse(int[] array) {
		for (int i = 0; i < array.length / 2; i++) {
			int temp = array[i];
			array[i] = array[array.length - i - 1];
			array[array.length - i - 1] = temp;
		}
	}

	public static int[] reverseArray(int[] array) {
		int[] reversedArray = new int[array.length];

		for (int i = 0, j = array.length - 1; i < array.length; i++, j--) {
			reversedArray[i] = array[j];
		}

		return reversedArray;
	}
}
