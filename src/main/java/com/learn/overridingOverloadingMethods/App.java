package com.learn.overridingOverloadingMethods;

/**
 * Hello world!
 *
 */
public class App {

	public static void main(String[] args) {

		System.out.println(displayHighScorePosition("Jim", calculateHighScorePosition(1500)));
		System.out.println(displayHighScorePosition("Tim", calculateHighScorePosition(900)));

		System.out.println(displayHighScorePosition("Geo", calculateHighScorePosition(400)));
		System.out.println(displayHighScorePosition("Darius", calculateHighScorePosition(50)));

	}

	public static String displayHighScorePosition(String playerName, int position) {
		return playerName + " managed to get into position " + position + " on the high score table.";
	}

	public static int calculateHighScorePosition(int playerScore) {

		if (playerScore >= 1000) {
			return 1;
		} else if (playerScore >= 500) {
			return 2;
		} else if (playerScore >= 100) {
			return 3;
		} else {
			return 4;
		}
	}
}