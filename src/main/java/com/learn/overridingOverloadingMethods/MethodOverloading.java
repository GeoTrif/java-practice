package com.learn.overridingOverloadingMethods;

public class MethodOverloading {

	public static void main(String[] args) {
		double inchesToTransform = 0;
		double feet = calcFeetAndInchesToCentimeters(inchesToTransform);
		double inches = 1;
		double centimeters = calcFeetAndInchesToCentimeters(feet, inches);

		System.out.println(feet + " feet and " + inches + " inches = " + centimeters + " centimeters.");

	}

	public static double calcFeetAndInchesToCentimeters(double feet, double inches) {

		if (feet >= 0 && inches >= 0 && inches <= 12) {
			double feetToInches = feet * 12;
			double totalInches = feetToInches + inches;

			return totalInches * 2.54;
		} else {
			return -1;
		}
	}

	public static double calcFeetAndInchesToCentimeters(double inches) {

		if (inches >= 0) {
			return inches / 12;
		} else {
			return -1;
		}
	}
}