package com.learn.overridingOverloadingMethods;

import java.util.Scanner;

public class ReadingUserInput {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int sum = 0;
		int counter = 1;
		int number = 0;

		System.out.println("The sum of the numbers is: " + countMethod1(scanner, sum, counter));
		// System.out.println("The sum of the numbers is: " + countMethod2(scanner, sum,
		// counter, number));
		scanner.close();
	}

	public static int countMethod1(Scanner scanner, int sum, int counter) {
		while (true) {
			System.out.println("Enter number #" + counter + ":");
			boolean isAnInt = scanner.hasNextInt();

			if (isAnInt) {
				int number = scanner.nextInt();
				counter++;
				sum += number;
				if (counter > 10) {
					break;
				}
			} else {
				System.out.println("Invalid number");
			}
			scanner.nextLine();
		}

		return sum;
	}

	public static int countMethod2(Scanner scanner, int sum, int counter, int number) {
		while (counter <= 10) {
			System.out.println("Enter number #" + counter + ":");
			if (scanner.hasNextInt()) {
				number = scanner.nextInt();
				sum += number;
				counter++;
			} else {
				System.out.println("Invalid number.");
			}
			scanner.nextLine();
		}

		return sum;
	}
}