package com.learn.overridingOverloadingMethods;

import java.util.Scanner;

public class MinAndMax {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		boolean flag = true;

		minAndMaxMethod1(scanner, min, max, flag);
		scanner.close();
	}

	public static void minAndMaxMethod1(Scanner scanner, int min, int max, boolean flag) {
		while (flag) {
			System.out.println("Enter a number(to exit enter a non numeric value):");
			if (scanner.hasNextInt()) {
				int number = scanner.nextInt();

				if (min > number) {
					min = number;
				}
				if (max < number) {
					max = number;
				}
			} else {
				System.out.println("Breaking loop....");
				flag = false;
			}
			scanner.nextLine();
		}

		System.out.println("The minimum entered is: " + min);
		System.out.println("The maximum estered is: " + max);
	}
}