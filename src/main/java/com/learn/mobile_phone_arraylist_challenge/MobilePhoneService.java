package com.learn.mobile_phone_arraylist_challenge;

import java.util.Scanner;

public class MobilePhoneService {

	private MobilePhoneFunctionality mobilePhoneFunctionality = new MobilePhoneFunctionality();
	private Scanner scanner = new Scanner(System.in);

	public void printListOfContacts() {
		mobilePhoneFunctionality.printContactList();
	}

	public void addNewContact() {
		System.out.println("Enter name:");
		String name = scanner.nextLine();
		System.out.println("Enter phone number:");
		String phoneNumber = scanner.nextLine();
		mobilePhoneFunctionality.addContact(name, phoneNumber);
	}

	public void updateExistingContact() {
		System.out.println("Enter old name:");
		String oldName = scanner.nextLine();
		System.out.println("Enter old phone number:");
		String oldPhoneNumber = scanner.nextLine();
		System.out.println("Enter new name:");
		String newName = scanner.nextLine();
		System.out.println("Enter new phone number:");
		String newPhoneNumber = scanner.nextLine();
		mobilePhoneFunctionality.updateContact(oldName, oldPhoneNumber, newName, newPhoneNumber);
	}

	public void removeContact() {
		System.out.println("Enter name:");
		String name = scanner.nextLine();
		mobilePhoneFunctionality.deleteContact(name);
	}

	public void findContact() {
		System.out.println("Enter name:");
		String name = scanner.nextLine();
		mobilePhoneFunctionality.findContact(name);
	}
}
