package com.learn.mobile_phone_arraylist_challenge;

/**
 * Create a program that implements a simple mobile phone with the following capabilities: able to store,modify,remove and query contact names. 
 * You will want to create a separate class for Contacts(name and phone number). 
 * Create a master class(MobilePhone) that holds the ArrayList of Contacs. 
 * The MobilePhone class has the functionality listed above. 
 * Add a menu of options that are available. 
 * Options: quit,print list of contacts,add new contact,update existing contact,remove contact and search/find contact. 
 * When adding or updating be sure to check if contact already exists(use name). 
 * Be sure not to expose inner workings of the ArrayList to MobilePhone 
 * e.g. no ints,no .get(i) etc. 
 * MobilePhone should do everything with Contact objects only.
 * 
 * @author geotrif
 *
 */
public class Main {

	private static MobilePhoneController mobilePhoneController = new MobilePhoneController();

	public static void main(String[] args) {
		mobilePhoneController.mainMenu();
	}
}
