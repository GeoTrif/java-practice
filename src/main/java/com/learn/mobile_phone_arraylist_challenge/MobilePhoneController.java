package com.learn.mobile_phone_arraylist_challenge;

import java.util.Scanner;

public class MobilePhoneController {

	private MobilePhoneService mobilePhoneService = new MobilePhoneService();
	private Scanner scanner = new Scanner(System.in);

	public void mainMenu() {

		while (true) {
			menu();
			System.out.println("Enter your choice:");
			int choice = scanner.nextInt();
			setupMainMenuFunctionality(choice);
		}
	}

	private void setupMainMenuFunctionality(int choice) {
		switch (choice) {
		case 1:
			mobilePhoneService.printListOfContacts();
			break;

		case 2:
			mobilePhoneService.addNewContact();
			break;

		case 3:
			mobilePhoneService.updateExistingContact();
			break;

		case 4:
			mobilePhoneService.removeContact();
			break;

		case 5:
			mobilePhoneService.findContact();
			break;

		case 6:
			menu();
			break;

		case 7:
			System.exit(0);
			break;

		default:
			System.out.println("Incorrect choice.");
		}
	}

	private void menu() {
		System.out.println("Mobile Phone");
		System.out.println("\t1 - Print list of contacts");
		System.out.println("\t2 - Add new contact");
		System.out.println("\t3 - Update existing contact");
		System.out.println("\t4 - Remove contact");
		System.out.println("\t5 - Find contact");
		System.out.println("\t6 - Print main menu");
		System.out.println("\t7 - Quit application");
	}
}
