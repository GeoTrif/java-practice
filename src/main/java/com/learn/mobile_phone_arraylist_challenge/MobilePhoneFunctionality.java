package com.learn.mobile_phone_arraylist_challenge;

import java.util.ArrayList;
import java.util.List;

public class MobilePhoneFunctionality {

	private List<Contact> contactsList = new ArrayList<>();

	public void printContactList() {
		for (Contact contact : contactsList) {
			System.out.println(contact.getName() + " : " + contact.getPhoneNumber());
		}
	}

	public void addContact(String name, String phoneNumber) {
		Contact contact = new Contact(name, phoneNumber);

		if (contactsList.isEmpty()) {
			contactsList.add(contact);
			System.out.println("Contact added.");
		} else {
			if (contactsList.contains(contact)) {
				System.out.println("Contact already in memory.");
			} else {
				contactsList.add(contact);
				System.out.println("Contact added.");
			}
		}
	}

	public void updateContact(String oldName, String oldPhoneNumber, String newName, String newPhoneNumber) {
		for (Contact contact : contactsList) {
			if (contact.getName().equals(oldName) && contact.getPhoneNumber().equals(oldPhoneNumber)) {
				contact.setName(newName);
				contact.setPhoneNumber(newPhoneNumber);
				System.out.println("Contact updated from " + oldName + ":" + oldPhoneNumber + " to " + newName + ":"
						+ newPhoneNumber);
			} else {
				System.out.println("Cannot find contact.");
			}
		}
	}

	public void deleteContact(String name) {
		for (Contact contact : contactsList) {
			if (contact.getName().equals(name)) {
				contactsList.remove(contact);
				System.out.println("Contact removed.");
			} else {
				System.out.println("Cannot find contact.");
			}
		}
	}

	public void findContact(String name) {
		Contact contact = new Contact(name);

		if (contactsList.contains(contact)) {
			System.out.println("Contact found in list.");
		} else {
			System.out.println("Cannot find contact.");
		}
	}
}
