package com.learn.oop2;

public class Door {

	private double width;
	private double height;
	private String material;

	public Door(double width, double height, String material) {
		this.width = width;
		this.height = height;
		this.material = material;
	}

	public void openDoor() {
		System.out.println("Opening door with dimensions: " + width + " x " + height + ",made from " + material);
	}
}
