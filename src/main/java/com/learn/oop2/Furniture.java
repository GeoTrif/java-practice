package com.learn.oop2;

public class Furniture {

	private String name;
	private double width;
	private double height;
	private double depth;
	private String material;
	private String colour;

	public Furniture(String name, double width, double height, double depth, String material, String colour) {
		this.name = name;
		this.width = width;
		this.height = height;
		this.depth = depth;
		this.material = material;
		this.colour = colour;
	}

	public void designFurniture() {
		System.out.println("The furniture is: " + name + ",with width = " + width + "height = " + height + ",depth = "
				+ depth + ",made from " + material + ",colour " + colour);
	}
}
