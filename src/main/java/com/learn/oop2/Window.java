package com.learn.oop2;

public class Window {

	private double width;
	private double height;
	private String material;

	public Window(double width, double height, String material) {
		this.width = width;
		this.height = height;
		this.material = material;
	}

	public void openWindow() {
		System.out.println("Opening window with dimensions: " + width + " x " + height + ",made from " + material);
	}
}
