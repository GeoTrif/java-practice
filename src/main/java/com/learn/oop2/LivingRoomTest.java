package com.learn.oop2;

public class LivingRoomTest {

	public static void main(String[] args) {

		Door door = new Door(110, 210, "wood");
		Window window = new Window(120, 120, "PVC");
		Furniture furniture = new Furniture("table", 100, 200, 5, "wood", "brown");
		LivingRoom livingRoom = new LivingRoom(door, window, furniture);

		livingRoom.designLivingRoom();
	}
}
 