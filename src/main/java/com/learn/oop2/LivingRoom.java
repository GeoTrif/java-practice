package com.learn.oop2;

public class LivingRoom {

	private Door door;
	private Window window;
	private Furniture furniture;

	public LivingRoom(Door door, Window window, Furniture furniture) {
		this.door = door;
		this.window = window;
		this.furniture = furniture;
	}

	public void designLivingRoom() {
		door.openDoor();
		window.openWindow();
		furniture.designFurniture();
	}

}
