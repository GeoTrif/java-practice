package com.learn.oop2;

public class PrinterTest {

	public static void main(String[] args) {

		Printer duplexPrinter = new Printer(20, true);
		duplexPrinter.fillToner(30);
		duplexPrinter.fillToner(100);
		duplexPrinter.printPage(11);
		duplexPrinter.printPage(100);
		duplexPrinter.printPage(10000);

		System.out.println("----------------------------");

		Printer normalPrinter = new Printer(20, false);
		normalPrinter.fillToner(30);
		normalPrinter.fillToner(100);
		normalPrinter.printPage(11);
		normalPrinter.printPage(100);
	}
}
