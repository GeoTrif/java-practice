package com.learn.oop2;

public class Printer {

	private int tonerLevel;
	private int numberOfPagesPrinted;
	private boolean duplexPrinter;

	public Printer(int tonerLevel, boolean isDuplexPrinter) {
		if (tonerLevel >= 0 && tonerLevel <= 100) {
			this.tonerLevel = tonerLevel;
		}
		this.numberOfPagesPrinted = 0;
		this.duplexPrinter = isDuplexPrinter;
	}

	public void fillToner(int amount) {
		this.tonerLevel += amount;

		if (this.tonerLevel >= 0 && this.tonerLevel <= 100) {
			System.out.println("Toner is filled " + this.tonerLevel + "%.");
		} else {
			this.tonerLevel = 100;
			System.out.println("Toner filler up at maximum " + this.tonerLevel + "%.");
		}
	}

	public void printPage(int numberOfPages) {
		this.tonerLevel -= (0.1 * numberOfPages);
		int pages = numberOfPages;

		if (this.tonerLevel <= 0) {
			this.tonerLevel = 0;
			System.out.println("Toner depleted.Please recharge toner.");
		} else {
			System.out.println("Toner level at " + this.tonerLevel + "%.");
			this.numberOfPagesPrinted += numberOfPages;

			if (this.duplexPrinter) {
				pages = (numberOfPages / 2) + (numberOfPages % 2);
				System.out.println("The printer is a duplex one and has printed " + pages + " pages.");
			} else {
				System.out.println("The printer has printed " + pages + " pages.");
			}
		}

		System.out.println("Total pages printed so far is " + this.numberOfPagesPrinted);
	}

	public int getTonerLevel() {
		return tonerLevel;
	}

	public int getNumberOfPagesPrinted() {
		return numberOfPagesPrinted;
	}

	public boolean isDuplexPrinter() {
		return duplexPrinter;
	}
}
