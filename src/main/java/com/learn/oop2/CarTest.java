package com.learn.oop2;

import java.util.Random;

class Car {
	private String name;
	private boolean engine;
	private int cylinders;
	private int wheels;

	public Car(String name, int cylinders) {
		this.name = name;
		this.engine = true;
		this.cylinders = cylinders;
		this.wheels = 4;
	}

	public String getName() {
		return name;
	}

	public int getCylinders() {
		return cylinders;
	}

	public void startEngine() {
		System.out.println("Starting cars engine.");
	}

	public void accelerate() {
		System.out.println("Accelerating car.");
	}

	public void brake() {
		System.out.println("Braking car");
	}
}

class Porsche extends Car {

	public Porsche() {
		super("Porsche", 4);
	}

	@Override
	public void startEngine() {
		System.out.println("Starting Porsche engine.");
	}

	@Override
	public void accelerate() {
		System.out.println("Porsche is accelerating to 220km/h in 5 seconds.");
	}

	@Override
	public void brake() {
		System.out.println("Porsche car is braking in 1.5 seconds.");
	}
}

class Kia extends Car {

	public Kia() {
		super("Kia", 4);
	}

	@Override
	public void startEngine() {
		System.out.println("Starting Kia engine.");
	}

	@Override
	public void accelerate() {
		System.out.println("Kia is accelerating to 180km/h in 4 seconds.");
	}

	@Override
	public void brake() {
		System.out.println("Kia car is braking in 2 seconds.");
	}
}

class Opel extends Car {

	public Opel() {
		super("Opel", 4);
	}

	@Override
	public void startEngine() {
		System.out.println("Starting Opel engine.");
	}

	@Override
	public void accelerate() {
		System.out.println("Opel is accelerating to 140km/h in 6 seconds.");
	}

	@Override
	public void brake() {
		System.out.println("Opel car is braking in 2.2 seconds.");
	}
}

class GenericCar extends Car {

	public GenericCar() {
		super("Generic Car", 4);
	}
}

public class CarTest {

	public static void main(String[] args) {
		for (int i = 1; i <= 10; i++) {
			Car car = randomCar();

			System.out.println("Car is " + car.getName());
			car.startEngine();
			car.accelerate();
			car.brake();
			System.out.println("-------------------");
		}
	}

	public static Car randomCar() {
		Random random = new Random();
		int randomNumber = random.nextInt(5) + 1;

		switch (randomNumber) {
		case 1:
			return new Porsche();

		case 2:
			return new Kia();

		case 3:
			return new Opel();

		default:
			return new GenericCar();
		}
	}
}
