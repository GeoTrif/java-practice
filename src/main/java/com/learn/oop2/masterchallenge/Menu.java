package com.learn.oop2.masterchallenge;

public class Menu {

	public void printMainMenu() {
		System.out.println("Welcome to Bills Burger.");
		System.out.println("Please select a choice");
		System.out.println("1 - Menu");
		System.out.println("2 - Exit");
	}

	public void printBurgerMenu() {
		System.out.println("Bills Burger Menu");
		System.out.println("1 - Classic Burger");
		System.out.println("2 - Healthy Burger");
		System.out.println("3 - Deluxe Burger");
		System.out.println("4 - Back to Main Menu");
		System.out.println("5 - Exit application");
	}

	public void printHamburgerMenu() {
		System.out.println("Classic Hamburger");
		System.out.println("The classic hamburger can have up to 4 additional items added.");
		System.out.println("1 - Bread roll");
		System.out.println("2 - Meat");
		System.out.println("3 - Add extras");
		System.out.println("4 - See all additions with pricing");
		System.out.println("5 - See current burger additions and pricing");
		System.out.println("6 - Order burger");
		System.out.println("7 - Back to BurgerMenu");
		System.out.println("8 - Exit application");
	}

	public void printHealthyBurgerMenu() {
		System.out.println("Healthy Burger");
		System.out.println("The healthy burger has can have up to 2 additional items added.");
		System.out.println("1 - Meat");
		System.out.println("2 - Add extras");
		System.out.println("3 - Order burger");
		System.out.println("4 - Back to BurgerMenu");
		System.out.println("5 - Exit application");
	}

	public void printDeluxeBurgerMenu() {
		System.out.println("Deluxe Burger");
		System.out.println("The deluxe burger has fries and a drink included.");
		System.out.println("1 - Order burger");
		System.out.println("2 - Back to BurgerMenu");
		System.out.println("3 - Exit application");
	}

	public void printBreadRollsMenu() {
		System.out.println("Bread rolls");
		System.out.println("Choose  bread roll:");
		System.out.println("1 - White bread roll");
		System.out.println("2 - Olives bread roll");
		System.out.println("3 - Egg bun");
		System.out.println("4 - Mexican roll");
	}

	public void printMeatMenu() {
		System.out.println("Hamburger meat");
		System.out.println("Choose hamburger patty:");
		System.out.println("1 - Beef");
		System.out.println("2 - Chicken");
		System.out.println("3 - Pork");
	}

	public void printAdditionsMenu() {
		System.out.println("Additions");
		System.out.println("Choose additional extras:");
		System.out.println("1 - Lettuce");
		System.out.println("2 - Tomatoes");
		System.out.println("3 - Onions");
		System.out.println("4 - Extra cheese");
		System.out.println("5 - Bacon");
		System.out.println("6 - Corn");
		System.out.println("7 - Ketchup");
		System.out.println("8 - Mustard");
		System.out.println("9 - Carrots");
	}
}
