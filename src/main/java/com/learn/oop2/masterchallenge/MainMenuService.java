package com.learn.oop2.masterchallenge;

public class MainMenuService {

	private Menu menu = new Menu();
	private BurgerMenuService burgerMenuService;
	private ScannerUtil scannerUtil = new ScannerUtil();
	private boolean flag;

	public void defineMainMenuFunctionality() {
		burgerMenuService = new BurgerMenuService();
		while (!flag) {
			menu.printMainMenu();
			System.out.println("Enter your choice:");
			int choice = scannerUtil.intScanner();
			selectMainMenuChoice(choice);
		}
	}

	private void selectMainMenuChoice(int choice) {
		switch (choice) {
		case 1:
			BurgerMenuService.defineBurgerMenuFunctionality();
			break;

		case 2:
			System.out.println("Exiting application...");
			flag = true;
			break;

		default:
			System.out.println("Incorrect choice.");
			defineMainMenuFunctionality();
		}
	}
}
