package com.learn.oop2.masterchallenge;

import java.util.Scanner;

public class ScannerUtil {

	Scanner scanner = new Scanner(System.in);

	public int intScanner() {
		return scanner.nextInt();
	}

	public double doubleScanner() {
		return scanner.nextDouble();
	}

	public String stringScanner() {
		return scanner.next();
	}
}
