package com.learn.oop2.masterchallenge;

import java.util.HashMap;
import java.util.Map;

public class Hamburger {

	private String breadRollType;
	private String meat;
	private double price = 10;

	public Hamburger() {

	}

	public Hamburger(String breadRollType, String meat) {
		this.breadRollType = breadRollType;
		this.meat = meat;
	}

	public String getBreadRollType() {
		return breadRollType;
	}

	public void setBreadRollType(String breadRollType) {
		this.breadRollType = breadRollType;
	}

	public String getMeat() {
		return meat;
	}

	public void setMeat(String meat) {
		this.meat = meat;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Map<String, Double> defineAdditions() {
		Map<String, Double> additions = new HashMap<>();
		additions.put("Lettuce", 1.00);
		additions.put("Tomatoes", 1.00);
		additions.put("Onions", 0.5);
		additions.put("Extra cheese", 2.00);
		additions.put("Bacon", 1.50);
		additions.put("Corn", 1.20);
		additions.put("Ketchup", 0.75);
		additions.put("Mustard", 0.40);
		additions.put("Carrots", 0.70);

		return additions;
	}
}
