package com.learn.oop2.masterchallenge;

public class BurgerMenuService {

	private static Menu menu = new Menu();
	private static ScannerUtil scannerUtil = new ScannerUtil();
	private static MainMenuService mainMenuService = new MainMenuService();
	private static HamburgerService hamburgerService = new HamburgerService();

	public static void defineBurgerMenuFunctionality() {
		while (true) {
			menu.printBurgerMenu();
			System.out.println("Enter your choice:");
			int choice = scannerUtil.intScanner();
			selectBurgerMenuChoice(choice);
		}
	}

	private static void selectBurgerMenuChoice(int choice) {
		switch (choice) {
		case 1:
			hamburgerService.defineHamburgerMenuFunctionality();
			break;

		case 2:
			System.out.println("Healthy Burger selected.");
			break;

		case 3:
			System.out.println("Deluxe Burger selected.");
			break;

		case 4:
			mainMenuService.defineMainMenuFunctionality();
			break;

		case 5:
			System.out.println("Exiting application...");
			System.exit(0);
			break;

		default:
			System.out.println("Incorrect choice.");
			defineBurgerMenuFunctionality();
		}
	}

}
