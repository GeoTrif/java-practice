package com.learn.oop2.masterchallenge;

import java.util.HashMap;
import java.util.Map;

public class HamburgerService {

	private static final int MAXIMUM_ADDITIONS_PER_BURGER = 4;
	private static final String ENTER_YOUR_CHOICE_STRING = "Enter your choice:";

	private Hamburger hamburger = new Hamburger();
	private Menu menu = new Menu();
	private ScannerUtil scannerUtil = new ScannerUtil();
	private Map<String, Double> selectedAdditions = BurgerCreatorUtil.selectedAdditions;

	public void defineHamburgerMenuFunctionality() {
		while (true) {
			menu.printHamburgerMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int choice = scannerUtil.intScanner();
			selectHamburgerChoice(choice);
		}
	}

	public void selectHamburgerChoice(int choice) {
		switch (choice) {
		case 1:
			menu.printBreadRollsMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int breadRollChoice = scannerUtil.intScanner();
			BurgerCreatorUtil.addBreadRoll(breadRollChoice, hamburger);
			break;

		case 2:
			menu.printMeatMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int meatChoice = scannerUtil.intScanner();
			BurgerCreatorUtil.addMeatPatty(meatChoice, hamburger);
			break;

		case 3:
			menu.printAdditionsMenu();
			System.out.println(ENTER_YOUR_CHOICE_STRING);
			int additionsChoice = scannerUtil.intScanner();
			if (BurgerCreatorUtil.additionsCounter < MAXIMUM_ADDITIONS_PER_BURGER) {
				BurgerCreatorUtil.addAdditions(additionsChoice, hamburger);
			} else {
				System.out.println("Maximum 4 additions.You can't have more.");
			}
			break;

		case 4:
			System.out.println("Additions pricing");
			Map<String, Double> additions = hamburger.defineAdditions();
			for (Map.Entry<String, Double> addition : additions.entrySet()) {
				System.out.println(addition.getKey() + " -> " + addition.getValue());
			}
			break;

		case 5:
			System.out.println("Your burger has the following items:");
			System.out.println("Bread Roll -> " + hamburger.getBreadRollType());
			System.out.println("Meat -> " + hamburger.getMeat());
			System.out.println("Additions -> " + selectedAdditions.size() + " additions selected; "
					+ (MAXIMUM_ADDITIONS_PER_BURGER - selectedAdditions.size()) + " additions available.");
			for (Map.Entry<String, Double> addition : selectedAdditions.entrySet()) {
				System.out.println(addition.getKey() + " -> " + addition.getValue());
			}
			System.out.println("Total price of the burger: " + hamburger.getPrice() + "$.");
			break;

		case 6:
			System.out.println("Order sended.Yor order is priced at " + hamburger.getPrice() + "$.");
			break;

		case 7:
			BurgerMenuService.defineBurgerMenuFunctionality();
			break;

		case 8:
			System.exit(0);
		}
	}

}
