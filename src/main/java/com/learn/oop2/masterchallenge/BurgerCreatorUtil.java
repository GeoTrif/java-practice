package com.learn.oop2.masterchallenge;

import java.util.HashMap;
import java.util.Map;

public class BurgerCreatorUtil {

	public static Map<String, Double> selectedAdditions = new HashMap<>();
	public static int additionsCounter;

	public static void addBreadRoll(int choice, Hamburger hamburger) {
		switch (choice) {
		case 1:
			hamburger.setBreadRollType("White bread roll");
			break;

		case 2:
			hamburger.setBreadRollType("Olives bread roll");
			break;

		case 3:
			hamburger.setBreadRollType("Egg bun");
			break;

		case 4:
			hamburger.setBreadRollType("Mexican roll");
			break;
		}
	}

	public static void addMeatPatty(int choice, Hamburger hamburger) {
		switch (choice) {
		case 1:
			hamburger.setMeat("Beef");
			break;

		case 2:
			hamburger.setMeat("Chicken");
			break;

		case 3:
			hamburger.setMeat("Pork");
			break;
		}
	}

	public static void addAdditions(int choice, Hamburger hamburger) {
		Map<String, Double> additions = hamburger.defineAdditions();

		switch (choice) {
		case 1:
			String lettuce = "Lettuce";
			hamburger.setPrice(hamburger.getPrice() + additions.get(lettuce));
			selectedAdditions.put(lettuce, additions.get(lettuce));
			break;
		case 2:
			String tomatoes = "Tomatoes";
			hamburger.setPrice(hamburger.getPrice() + additions.get(tomatoes));
			selectedAdditions.put(tomatoes, additions.get(tomatoes));
			break;
		case 3:
			String onions = "Onions";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Onions"));
			selectedAdditions.put(onions, additions.get(onions));
			break;
		case 4:
			String extraCheese = "Extra cheese";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Extra cheese"));
			selectedAdditions.put(extraCheese, additions.get(extraCheese));
			break;
		case 5:
			String bacon = "Bacon";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Bacon"));
			selectedAdditions.put(bacon, additions.get(bacon));
			break;
		case 6:
			String corn = "Corn";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Corn"));
			selectedAdditions.put(corn, additions.get(corn));
			break;
		case 7:
			String ketchup = "Ketchup";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Ketchup"));
			selectedAdditions.put(ketchup, additions.get(ketchup));
			break;
		case 8:
			String mustard = "Mustard";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Mustard"));
			selectedAdditions.put(mustard, additions.get(mustard));
			break;
		case 9:
			String carrots = "Carrots";
			hamburger.setPrice(hamburger.getPrice() + additions.get("Carrots"));
			selectedAdditions.put(carrots, additions.get(carrots));
			break;
		}

		additionsCounter++;
	}

}
