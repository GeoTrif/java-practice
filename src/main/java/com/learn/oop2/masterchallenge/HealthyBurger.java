package com.learn.oop2.masterchallenge;

public class HealthyBurger extends Hamburger {

	public HealthyBurger() {

	}

	public HealthyBurger(String meat) {
		super("Brown Rye Bread Roll", meat);
	}
}
